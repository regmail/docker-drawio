# draw.io

draw.io, diagrams.net est un logiciel de dessin graphique multiplateforme gratuit et open source développé en HTML5 et JavaScript. Son interface peut être utilisée pour créer des diagrammes tels que des organigrammes, des structures filaires, des diagrammes UML, des organigrammes et des diagrammes de réseau. Wikipédia

#Prerequis 

Utiliser docker-traefic

```
git clone git@gitlab.com:regmail/docker-traefik.git
```

# Configurer la stack 

Lancer le script config.sh

```
./config.sh
```
Pour voir les logs
```
docker-compose logs -f
```

Remarque : Les valeurs entre [] sont les valeurs prise par default sur on fait enter.


# Lancer la stack dokuwiki

``` 
docker-compose up -d 
```


