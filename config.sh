#!/usr/bin/env bash

cp ./docker-compose.yml.org ./docker-compose.yml

#echo "Introduire le nom de domaine (ex : drawio.domaine.com) :"
read -p "Introduire le nom de domaine [drawio.linregs.be] : " var_domaine 
var_domaine=${var_domaine:-drawio.linregs.be}


#echo "Introduire le nom du service drawio (ex : c_drawio) :"
read -p "Introduire le nom du service draw [c_drawio] : " var_c_name
var_c_name=${var_c_name:-c_drawio}


function generatePassword() {
    openssl rand -hex 16
}

#var_db_password=$(generatePassword)
#MARIADB_ROOT_PASSWORD=$(generatePassword)
#MARIADB_PASSWORD=$(generatePassword)

sed -i.`date +\%G\%m\%d-\%Hh\%Mm\%Ss`.bak \
    -e "s/var_c_name/${var_c_name}/g" \
    -e "s/var_domaine/${var_domaine}/g" \
    "$(dirname "$0")/docker-compose.yml"

echo "Le fichier docker-compose.yml a été généré"
chmod 640 ./docker-compose.yml
